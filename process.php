<?php
/*
*       Prepr Dalet Parser.
*       Door Dennis in 2020.
*       Lekker zender met die hap!
*/

/* Variabelen
---------------------------------------------------------------------------------------------- */
$settings = json_decode(file_get_contents("settings.json"));

$dbdsn = $settings->dbdsn;
$dbuser = $settings->dbuser;
$dbpass = $settings->dbpass;
$radio_id = $settings->radio_id;
$api_url = $settings->api_url;
$accesstoken = $settings->accesstoken;
$buffer = str_repeat(" ", 4096)."\n";


/* FUNCTIONS
---------------------------------------------------------------------------------------------- */

/*      Wat?        Een functie om de playlist per uur uit de LogEditor te halen.
        Hoe?        De functie connect met de DB en zet titles op een rij en filtert de jingles eruit.
        Dependency? Ja. Deze functie connect met de ODBC bron: DALET51 (de Dalet Database).
        Parameter?  1: De datum (Y-m-d H). 2: radio_id, de id van het specifieke station wat je zoekt
        Return?     Een array met daarin de huidige playlist van het uur.                           */

function get_playlist($date)
{
    global $radio_id, $dbdsn, $dbuser, $dbpass;

    $playlist = array(); //Initieer de array die wordt gereturned
    $conn = odbc_connect($dbdsn, $dbuser, $dbpass); //Connect naar de db

    $sql = "SELECT titles.interpret AS artist, titles.title, titles_references.reference_text FROM clocks
        JOIN clock_versions ON clocks.clock_id = clock_versions.clock_id
        JOIN items ON clock_versions.block_id = items.block_id
        JOIN spots ON items.item_id = spots.item_id
        JOIN titles ON spots.title_id = titles.title_id
        JOIN titles_references ON titles_references.title_id = titles.title_id
        WHERE clocks.clock_date='".$date."' AND clocks.radio_id = 11 AND reference_id = 2 AND titles.interpret != ''
        ORDER BY items.item_order";
    $rs = odbc_exec($conn, $sql); //Hier haal je de clock_id op
    while (odbc_fetch_row($rs)) {
        $title = odbc_result($rs, "title"); //Tot slot zoek je de title van de title (de naam)
        $artist = odbc_result($rs, "artist");
        $reference_text = odbc_result($rs, "reference_text");
        if (substr($title, 0, 2) != "  " && substr($title, 0, 4) != "TOTH") {
            array_push($playlist, array("time" => substr($date, 11, 2) . ":00:00", "artist" => utf8_encode($artist), "title" => utf8_encode($title), "reference_id" => $reference_text));  //Als het geen jingle is of een TOTH voeg je hem toe aan de array
        }
    }
    odbc_close($conn);
    return $playlist;
}



/*      Wat?        Een functie om de playlist per dag uit de LogEditor te halen
        Hoe?        Hij gebruiktde get_playlist het per uur kan doen en loopt er 24x doorheen
        Dependency? Ja. get_playlist().
        Parameter?  1: De datum (Y-m-d H). 2: radio_id, de id van het specifieke station wat je zoekt
        Return?     Een array met daarin alle tracks van de gespecificeerde dag                        */

function get_day_playlist($date)
{
    global $radio_id;
    $day_playlist = array();

    for ($x = 0; $x < 24; $x++) {
        $hour = sprintf("%02s", $x);
        $hour_playlist = get_playlist($date . " " . $hour, $radio_id);
        $day_playlist = array_merge($day_playlist, $hour_playlist);
    }

    return $day_playlist;
}






/*      Wat?        Deze functie pust een JSON in het juiste format in de API van Prepr.
        Hoe?        Doormiddel van een HTTP POST request naar de API van Prepr. 
        Dependency? Ja. De accesstoken en de juiste URL van de API.
        Parameter?  1: $playlist, deze verwacht een json die hij naar de API kan pushen.
        Return?     nulll                                                                      */

function api_request($playlist)
{
    global $api_url, $accesstoken;
    $url = $api_url . $accesstoken;


    $ch = curl_init($url);
    // Header to send
    $header = ['Content-Type:application/json'];

    // Set cURL options
    curl_setopt_array($ch, [
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $playlist,
        CURLOPT_HTTPHEADER => $header
    ]);

    // Execute the request and save the response
    //echo "gaatie<br>\n";
	$response = curl_exec($ch);
	//echo "gedaan<br>\n";
	//echo "<pre>";
	//print_r($response);
	//echo "</pre>";
    // Check for errors
    if (!$response) {
		// Er gaat iets mis bij Prepr: ze sturen een lege response. Daarom geeft de errorhandler hieronder terecht een fout.
		// Omdat het script hierdoor nodeloos wordt gestopt, staat de errorhandler uit. Huilen doe je maar bij je moeder.
		// die('Resultaat: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch) . "<br>\n");
    }

    // Decode the received JSON
    $received_data = json_decode($response);
	echo $received_data;
    // Close session to clear up resources
    curl_close($ch);
}


//OVERIGE FUNCTIES


/*      Wat?        Een functie die de naam van een specifieke clock kan ophalen.
        Hoe?        Hij checkt de naam van de clock in de DB van Dalet.
        Dependency? Ja. De ODBC bron DALET51 met credentials: dalet_admin/radioheld
        Parameter?  1: Het uur (YYYY-MM-DD HH) 2: radio_id (Studio040 = 10, Glow FM = 11)
        Return?     De naam die je zocht                                                */

function get_clock_name($date)
{
    global $radio_id, $dbdsn, $dbuser, $dbpass;
    $conn = odbc_connect($dbdsn, $dbuser, $dbpass); //Connect naar de db

    $sql = "SELECT * FROM clocks WHERE clock_date = '$date' AND radio_id = '$radio_id'";
    $rs = odbc_exec($conn, $sql); //Hier haal je de clock_id op
    $clock_id = odbc_result($rs, "clock_id");

    $sql_clockversion = "SELECT * FROM clock_versions WHERE clock_id = '$clock_id'";
    $rs_clockversion = odbc_exec($conn, $sql_clockversion); //Hier haal je het Block ID op
    $name = odbc_result($rs_clockversion, "name");
    return $name;
}



//---------------------------------------------------------------------------------

/*      Wat?        Een functie die kijkt voor welke dagen er nog playlisten zijn.
        Hoe?        Hij checkt de huidige datum en de hoogte datum in de database.
        Dependency? Ja. De datum van de apache machine en ODBC bron DALET51
        Parameter?  1: radio_id, de id van het specifieke station wat je zoekt
        Return?     Een array met daarin vandaag + alle andere mogelijke data    */

function date_check()
{
    global $radio_id, $dbdsn, $dbuser, $dbpass;
    $clock_dates = array();
    $date = date("Y-m-d");
    $conn = odbc_connect($dbdsn, $dbuser, $dbpass); //Connect naar de db
    $sql = "SELECT * FROM clocks WHERE radio_id = '$radio_id' AND clock_date > '$date'";
    $rs = odbc_exec($conn, $sql); //Hier haal je de clock_id op
    while (odbc_fetch_row($rs)) {
        $clock_date = substr(odbc_result($rs, "clock_date"), 0, 10);
        if (!in_array($clock_date, $clock_dates)) {
            array_push($clock_dates, $clock_date);
        }
    }
    return $clock_dates;
}



/*
                    Run code
-----------------------------------------------------------------------------------------

*/

ob_implicit_flush(true);
ob_end_flush();

if ($_SERVER["REQUEST_METHOD"] == "POST") { //Alleen als er data in de POST staat runt hij deze code
    $days = $_POST['day']; //De data uit de POST ophalen.
    $hours = $_POST['hour'];

    if (!empty($days)) { //Als $days niet leeg is, dan runt hij deze code.

        echo "<h1>Dalet to Prepr (door Dennis)</h1>";
        foreach ($days as $day){
            echo "<h2>".$day."</h2>";

            $array = get_day_playlist($day); //De playlist van de geselecteerde dag wordt opgehaald
            echo "<p>Dalet OK</p>";

            $json = json_encode(array("date" => $day, "tracks" => $array)); //De data wordt gepushed naar het juiste format
            api_request($json); //Je kunt maar 1 dag per API request inschieten in Prepr, daarom BINNEN de foreach loop.
            echo "<p>Prepr OK</p>";
        }
        echo "<h1>Zo. Dat was het al weer!";
    }

    if (!empty($hours)) {
        echo "<h1>Dalet to Prepr (door Dennis)</h1>";
        $date = substr($hours[0], 0, 10); //De eerste 10 tekens in de $hours van de eerste key vormen de datum
        $tracks = array();
        foreach ($hours as $hour) {
            echo "<p>".$hour."u OK</p>";
            $tracks = array_merge($tracks, get_playlist($hour)); //Voeg de tracks van dit uur toe aan de tracks array
        }
        $json = json_encode(array("date" => $date, "tracks" => $tracks)); //Vorm de juiste JSON voor de API
        api_request($json);
        echo "<p>Prepr OK</p>";
        echo "<h1>Zo. Dat was het al weer!</h1>";
    }
}