Hoi! 

INLEIDING
Welkom in mij Prepr Dalet Parser. Deze is geschreven in 2020 (ja midden in de coronacrisis) met als doel om de playlisten van Studio040 en Glow FM naar Prepr te kunnen pushen. Dit kan op 2 manieren, de lijsten uit Powergold kunnen worden geïmporteerd in Prepr of we kunnen een tool draaien die ze uit Dalet plukt en ook kan updaten als je aanpassingen hebt gemaakt aan je playlist... Je raad het waarschijnlijk al, het is dat tweede geworden. Waarom? Omdat het cool is én omdat je dan als DJ je playlist kunt bewerken in Dalet en hem vervolgens kunt pushen naar Prepr en hem dus kan updaten. Met deze reden zijn er ook 2 manieren om de playlist te pushen naar Dalet. De ene functie voor de muziekplanners van Glow FM en Studio040 waarmee zijn de playlisten kunnen pushen als er gepland is en de andere is ervoor om losse uren up te daten als deze zijn bewerkt door de jocks. Het zou niet zo handig zijn om dan de hele playlist te updaten, dan loop je het risico iemand anders zijn werk ongedaan te maken.



CONFIGURATIE EN INSTALLATIE
De software is makkelijk te configueren. Het werkt met zowel Dalet 5.1E als Omniplayer 2. Je download de scripts en zet ze ergens in een apache of Nginx htdocs map. Check of PHP is geïnstalleerd en of Curl is geactiveerd (dit doe je in het php.ini bestand). Vervolgens moet je even de credentials voor de DB aanmaken in een json file. Deze staat NIET in de git uit security overwegingen. Maak daarom settings.json aan met de volgende indeling:


{
	"dbdsn": "DSN",
	"dbuser": "USER",
	"dbpass": "PASS",
	"radio_id": "RADIO ID",
	"api_url": "API URL",
	"accesstoken": "ACCESSTOKEN"
}


De DSN gegevens zijn dezelfde als die van je Dalet of Omniplayer installatie. De radio_id kun je makkelijk achterhalen in Dalet DB Config of in de DB config van Omniplayer. Deze ID is belangrijk om te weten welke station er naar Prepr moet worden gepushed. Ten slotte voeg je ook nog een API url toe, dit is de url waar de API zich beving en ten slotte ook de accesstoken die je hebt aangemaakt in Prepr.



GEBRUIKSAANWIJZING
Als je dit hebt ingesteld ga je naar de webserver waar het script staat. Als je .php opent krijg je een lijst van alle dagen in de LogEditor en alle uren van die dag. Vervolgens kun je selecteren welke uren je wilt syncroniseren naar Prepr door ze aan te klikken en dan op versturen te klikken.

Mocht je hele dagen tegelijker tijd willen pushen dan doe je dit door op de knop "allemel doen" te drukken. Het leven is zo simpel!