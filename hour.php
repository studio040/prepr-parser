<?php

/*
*       Prepr playlist pusher
*       Door Dennis in 2020.
*       Lekker zender met die hap!
*/

include 'process.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Glow FM Prepr uur playlist pusher</title>
</head>

<body>
	<h1>Glow FM Prepr dag playlist pusher</h1>
	</script>
        <?php
        /*
        Voor elke dag krijg je een aparte form met alle uren van die dag en een submit. Je kunt 
        maar 1 dag tegelijkertijd pushen naar de API van Prepr namelijk. Voor mensen die uren
        van 2 verschillende dagen willen pushen open de tool maar 2 keer. :)
        */

        
        foreach (date_check() as $date) { //Een loop voor elke dag in de DB inclusief vandaag
            echo "<h2>" . $date . "</h2><br>"; 
            echo '<form method="post" action="process.php">';
            for ($x = 0; $x < 24; $x++) { //24 keer lopen, voor de 24 uren in een dag
                $hour = sprintf("%02s", $x); //Even parsen naar 2 digits (01 ipv 1, 02 ipv 2 ...)
                $date_time = $date . " " . $hour;
                $clockname = get_clock_name($date_time);
                echo '<input type="checkbox" name="hour[]" value="' . $date_time . '">' .$hour."u ".$clockname."<br></label>";
            }
            echo '<input type="submit"></form>';
        }
        ?>
</body>

</html>